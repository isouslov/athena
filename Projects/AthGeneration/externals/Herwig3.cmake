#
# File specifying the location of Herwig3 to use.
#

set( HERWIG3_LCGVERSION 7.2.0 )
# Comment the following line for H7 versions <7.2
set( HW3_VER_IS_72 1 )
set( HERWIG3_LCGROOT
   ${LCG_RELEASE_DIR}/MCGenerators/herwig++/${HERWIG3_LCGVERSION}/${LCG_PLATFORM} )
